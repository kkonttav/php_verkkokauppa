<?php include_once 'inc/top.php'; ?>

<?php 
    if($_SERVER["REQUEST_METHOD"] == "POST" && $tietokanta != null) /* Tarkista onko tietokannan avaus onnistunut */
        {
        try {
            $etunimi=filter_input(INPUT_POST,'etunimi',FILTER_SANITIZE_STRING);
            $sukunimi=filter_input(INPUT_POST,'sukunimi',FILTER_SANITIZE_STRING);
            $lahiosoite=filter_input(INPUT_POST,'lahiosoite',FILTER_SANITIZE_STRING);
            $postitoimipaikka=filter_input(INPUT_POST,'postitoimipaikka',FILTER_SANITIZE_STRING);
            $postinumero=filter_input(INPUT_POST,'postinumero',FILTER_SANITIZE_STRING);
            $sahkoposti=filter_input(INPUT_POST,'sahkoposti',FILTER_SANITIZE_STRING);
            $puhelinnumero=filter_input(INPUT_POST,'puhelinnumero',FILTER_SANITIZE_STRING);

            /* Aloita transaction jonka alle tehdään kaikki tarvittavat tietokanta insertoinnit */
            $tietokanta->beginTransaction();
            
            /* Asiakas tallennus */
            $kysely = $tietokanta->prepare("INSERT INTO asiakas (etunimi, sukunimi, lahiosoite, postitoimipaikka, postinumero, email, puhelin) "
                    . "VALUES (:etunimi, :sukunimi, :lahiosoite, :postitoimipaikka, :postinumero, :email, :puhelin)");
            $kysely->bindValue(':etunimi', $etunimi, PDO::PARAM_STR);
            $kysely->bindValue(':sukunimi', $sukunimi, PDO::PARAM_STR);
            $kysely->bindValue(':lahiosoite', $lahiosoite, PDO::PARAM_STR);
            $kysely->bindValue(':postitoimipaikka', $postitoimipaikka, PDO::PARAM_STR);
            $kysely->bindValue(':postinumero', $postinumero, PDO::PARAM_STR);
            $kysely->bindValue(':email', $sahkoposti, PDO::PARAM_STR);
            $kysely->bindValue(':puhelin', $puhelinnumero, PDO::PARAM_STR);            
            $kysely->execute();
            $asiakas_id=$tietokanta->lastInsertId();
            
            /* Tilaus tallennus */
            $kysely = $tietokanta->prepare("INSERT INTO tilaus (asiakas_id) "
                    . "VALUES (:asiakas_id)");
            $kysely->bindValue(':asiakas_id', $asiakas_id, PDO::PARAM_INT);
            $kysely->execute();
            $tilaus_id=$tietokanta->lastInsertId();

            /* Tilausrivien tallennus */
            foreach ($_SESSION['ostoskori'] as $tietue) {
                $kysely = $tietokanta->prepare("INSERT INTO tilausrivi (tilaus_id, tuote_id) "
                        . "VALUES (:tilaus_id, :tuote_id)");
                $kysely->bindValue(':tilaus_id', $tilaus_id, PDO::PARAM_INT);
                $kysely->bindValue(':tuote_id', $tietue->id, PDO::PARAM_INT);
                $kysely->execute();
                }
                
            /* kuittaa transactioni tehdyksi ja tyhjennä ostoskori */
            $tietokanta->commit();
            /* Tyhjennä == poista ja luo uusi */
            unset($_SESSION['ostoskori']);
            $_SESSION['ostoskori']=array();
            /* Tiedon tallennus onnistui --> Näytä SUCCESS viesti käyttäjälle */
            ?>
            <div class="alert alert-success" role="alert">
              <span class="sr-only"></span>                    
                Kiitos tilauksestasi!
            </div>
            <a href='index.php'>Takaisin tuotteisiin.</a>
            <?php

        } catch (PDOException $pdoex) {
            /* Tiedon tallennus epäonnistui */
            ?>
            <div class="alert alert-warning" role="alert">
            <span class="sr-only"></span>                    
            Jotain meni pieleen!!!
            <br />
            <?php
            print $pdoex->getMessage();
            ?>
            </div>
            <?php
            /* peruuta transactioni koska jokin pyynnöistä epäonnistui */
            $tietokanta->rollBack();
          }
        }
        else
            {
?>
            <div class="row">             
                <div class="col-xs-12 tilaus">
                    <div class="row">
                        <div class="col-xs-12">
                            <h4>Tilaus</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-8 col-lg-6">
                            <form role="form" action="order.php" method="post">
                                <div class="form-group">
                                    <label>Etunimi</label>
                                    <input name="etunimi" class="form-control" required autofocus>
                                </div>
                                <div class="form-group">
                                    <label>Sukunimi</label>
                                    <input name="sukunimi" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Lähiosoite</label>
                                    <input name="lahiosoite" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Postitoimipaikka</label>
                                    <input name="postitoimipaikka" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Postinumero</label>
                                    <input name="postinumero" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Sähköposti</label>
                                    <input name="sahkoposti" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Puhelinnumero</label>
                                    <input name="puhelinnumero" class="form-control" required>
                                </div>
                                <button type="submit" class="btn btn-primary">Lähetä</button>
                                <button type="reset" class="btn btn-default" onclick="window.location='index.php';return false;">Peruuta</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
<?php
            }
?>
            
<?php include_once 'inc/bottom.php'; ?>