<?php
session_start();
session_regenerate_id();
if(isset($_SESSION['ostoskori'])) {
    $clear=filter_input(INPUT_GET,'clear',FILTER_SANITIZE_NUMBER_INT);
    if($clear==1) {
        /* Tyhjennä == poista ja luo uusi */
        unset($_SESSION['ostoskori']);
        $_SESSION['ostoskori']=array();
        }    
    }
else {
    $_SESSION['ostoskori']=array();
    }

    
    
//session_unset(); /* Käytä tätä Session nollaamiseen testauksen holpottamiseksi!!! */
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <title>KaKo Store</title>
    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>        
            <a class="navbar-brand" href="index.php">Verkkokauppa</a>
            <ul class="nav navbar-nav">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    Ylläpito
                    <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="tuoteryhma.php">Lisää tuoteryhmä</a></li>
                  <li><a href="tuote.php">Lisää tuote</a></li>
                  <li><a href="tilaukset.php">Tilaukset</a></li>
                </ul>
              </li>
            </ul>                
            </div>  
          </div>
        </nav>
        <div class="container content">
            <?php
            /* Erillinen käsittely tietokannan avaukselle jotta voidaan eritellä myös virhe viestit. */
            try {
                $tietokanta = new PDO('mysql:host=localhost;dbname=verkkokauppa;charset=utf8','root','');      
                $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            } catch (Exception $pdoex) {
                    $tietokanta = null;
                    ?>
                    <div class="alert alert-danger" role="alert">
                      <span class="sr-only"></span>
                        Häiriö järjestelmässä. Tietokantaa ei voida avata.
                        <br />
                        <?php
                        print $pdoex->getMessage();
                        ?>
                    </div>
                    <?php                        
            }
            ?> 
