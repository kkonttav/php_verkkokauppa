<?php
    $summa = 0.00;
    foreach ($_SESSION['ostoskori'] as $tietue) {
        $summa=$summa + $tietue->hinta;
        }
?>
    <div class="col-xs-6" style="text-align: right;">
        <button class="btn btn-lg btn-info" data-toggle="collapse" data-target="#ostoskori">
            <span class="glyphicon glyphicon-shopping-cart ostoskorinappi"><?php printf("%.2f",$summa);?> eur</span>
        </button>
    </div>
    <div class="col-xs-12" style="text-align: right;">
        <div id="ostoskori" class="collapse ostoskori">
            <table>
                <?php
                    foreach ($_SESSION['ostoskori'] as $tietue) {
                        print "<tr>";
                        print "<td>$tietue->nimi</td>";
                        print "<td>$tietue->hinta</td>";
                        print "<td><span class='glyphicon glyphicon-trash'></span></td>";
                        print "</tr>";
                        }
                ?>                
                <tr class="summa">
                    <td>Summa</td>
                    <td>
                    <?php
                    printf("%.2f",$summa);
                    print " eur";
                    ?>
                    </td>
                    <td></td>
                </tr>
            </table>
            <?php
            if(count($_SESSION['ostoskori']) == 0) {
            ?>
                <button class="btn btn-primary" disabled title="Sinulla ei ole tuotteita korissa!" onclick="window.location="order.php">
                    Kassalle
                </button>
                <button class="btn btn-default" disabled title="Sinulla ei ole tuotteita korissa!" onclick="window.location='index.php?clear=1';return false;">
                    Tyhjennä
                </button>
            <?php
                }
            else {
            ?>
                <button class="btn btn-primary" title="Anna asiakastiedot ja tilaa ostoskorin tuotteet" onclick="window.location='order.php';return false;">
                    Kassalle
                </button>
                <button class="btn btn-default" title="Tyhjennä ostoskori!" onclick="window.location='index.php?clear=1';return false;">
                    Tyhjennä
                </button>
            <?php
            }
            ?>                
        </div>
    </div>

<?php

?>
