<?php include_once 'inc/top.php'; ?>

<?php 
    if($_SERVER["REQUEST_METHOD"] == "POST" && $tietokanta != null) /* Tarkista onko tietokannan avaus onnistunut */
        {
        include_once("apu.php");
        if ($_FILES['kuva']['error'] == UPLOAD_ERR_OK)
            {
            $kuva=$_FILES['kuva']['name'];
            $kuvannimi=$_FILES['kuva']['name']; /* Nimi talteen tietokantaa varten */
            if ($_FILES['kuva']['size'] > 0)
                { 
                $tyyppi=$_FILES['kuva']['type'];
                if (strcmp($tyyppi, "image/jpeg")==0 || str_cmp($tyyppi,"image/pjpeg")==0)
                    {
                    $kuva =  basename($kuva);
                    $kansio = 'tuotteet/';
                    make_thumb($_FILES['kuva']['tmp_name'], $kansio, 150,$kuva);
                    /* Kuvan tallenus onnistui jatka tuotteen tallentamista tietokantaan */
                    try {
                        $nimi=filter_input(INPUT_POST,'nimi',FILTER_SANITIZE_STRING);
                        $kuvaus=filter_input(INPUT_POST,'kuvaus',FILTER_SANITIZE_STRING);
                        $hinta=filter_input(INPUT_POST,'hinta',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
                        $tuoteryhma= filter_input(INPUT_POST,'tuoteryhma',FILTER_SANITIZE_NUMBER_INT);

                        $kysely = $tietokanta->prepare("INSERT INTO tuote (nimi, kuvaus, hinta, kuva, tuoteryhma_id) "
                                . "VALUES (:nimi, :kuvaus, :hinta, :kuva, :tuoteryhma_id)");

                        $kysely->bindValue(':nimi', $nimi, PDO::PARAM_STR);
                        $kysely->bindValue(':kuvaus', $kuvaus, PDO::PARAM_STR);
                        $kysely->bindValue(':hinta', $hinta, PDO::PARAM_STR);
                        $kysely->bindValue(':kuva', $kuvannimi, PDO::PARAM_STR);
                        $kysely->bindValue(':tuoteryhma_id', $tuoteryhma, PDO::PARAM_INT);
                        $kysely->execute();                        
                        
                        /* Tiedon tallennus onnistui --> Näytä SUCCESS viesti käyttäjälle */
                        ?>
                        <div class="alert alert-success" role="alert">
                          <span class="sr-only"></span>                    
                            Tuote <?php print "$nimi";?> tallennettu.
                        </div>
                        <?php

                    } catch (PDOException $pdoex) {
                        /* Tiedon tallennus epäonnistui --> tarkista syy */
                        /* Poista kuva tiedosto */
                        unlink("$kansio$kuva");
                        ?>
                        <div class="alert alert-danger" role="alert">
                          <span class="sr-only"></span>
                            Tuotteen tallennuksessa tapahtui virhe,
                            <br />
                            <?php
                            print $pdoex->getMessage();
                            ?>
                        </div>
                        <?php
                        }
                    } 
                else 
                    {
                    /* Kuva ei ole jpg --> näytä virhe viesti */
                    ?>
                    <div class="alert alert-danger" role="alert">
                      <span class="sr-only"></span>
                        Voit ladata vain jpg-kuvia.
                        <br />
                    </div>
                    <?php
                    }
                }
            else 
                {
                /* Kuva ei voida ladata --> näytä virhe viesti */
                ?>
                <div class="alert alert-danger" role="alert">
                  <span class="sr-only"></span>
                    Kuvaa ei voida ladata.
                    <br />
                </div>
                <?php
                }                 
            }
        else 
            {
            /* Kuva ei voida ladata --> näytä virhe viesti */
            ?>
            <div class="alert alert-danger" role="alert">
              <span class="sr-only"></span>
                Kuvaa ei voida ladata.
                <br />
            </div>
            <?php
            }    
        }
    ?>
    <h3>Lisää tuote</h3>
    <form role="form" action='<?php print($_SERVER['PHP_SELF']); ?>' method="post" enctype="multipart/form-data">
      <div class="form-group">
        <label for="tuoteryhma">Tuoteryhmä:</label>
<?php    
        if($tietokanta != null) {
            try {
                $sql='SELECT * FROM tuoteryhma';
                $kysely=$tietokanta->query($sql);
                $kysely->setFetchMode(PDO::FETCH_OBJ);        
        
            print "<select class='form-control' name=tuoteryhma>";
            while($tietue=$kysely->fetch()) {
                print "<option value='$tietue->id'>$tietue->nimi</option>";
            }
            print "</select>";
        } catch (Exception $pdoex) {
            /* Kirjoita virhe ilmoitus. Tulee dropdown listalle näkyviin kun käyttäjä sen valitsee. */
            print "Tietokannan luennassa tapahtui virhe <br />" . $pdoex->getMessage();
            }
        }    
?>        
      </div>        
      <div class="form-group">
        <label for="Nimi">Nimi:</label>
        <input type="text" class="form-control" name="nimi" maxlength="50" autofocus>
      </div>
      <div class="form-group">
        <label for="Kuvaus">Kuvaus:</label>
        <textarea type="text" class="form-control" name="kuvaus"></textarea>
      </div>
      <div class="form-group">
        <label for="Kuva">Kuvatiedosto:</label>
        <input type="file" name="kuva" maxlength="50"">
      </div>
      <div class="form-group">
        <label for="Hinta">Hinta:</label>
        <input type="number" step="any" class="form-control" name="hinta" maxlength="5">
      </div>        
      <button type="submit" class="btn btn-primary">Tallenna</button>
      <input type="button" class="btn btn-default" onclick="window.location='index.php';return false;" value='Peruuta'>
    </form>
<?php include_once 'inc/bottom.php'; ?>