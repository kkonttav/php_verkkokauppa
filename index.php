<?php include_once 'inc/top.php'; ?>

    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-xs-6">      
                    <div class="dropdown">
                        <button class="btn btn-default btn-lg dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" btn-lg dropdown-togglearia-haspopup="true" aria-expanded="true">
                          Tuotteet
                          <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <?php
                        /* Tuoteryhmän luenta. */
                        if($tietokanta != null) {
                            try {
                                $sql='SELECT * FROM tuoteryhma';
                                $kysely=$tietokanta->query($sql);
                                $kysely->setFetchMode(PDO::FETCH_OBJ);

                                while($tietue=$kysely->fetch()) {
                                    print "<li><a href=index.php?tuoteryhma_id=$tietue->id>$tietue->nimi</a></li>";
                                }

                            } catch (Exception $pdoex) {
                                /* Kirjoita virhe ilmoitus. Tulee dropdown listalle näkyviin kun käyttäjä sen valitsee. */
                                print "Tuoteryhmien luennassa tapahtui virhe <br />" . $pdoex->getMessage();
                                }
                        }
                        /* sulje aina htlml-lista. */
                        print "</u>";
                        ?>
                    </div>       
                </div>
                <?php include_once 'kori.php'; ?>
            </div>             
        </div>
    </div>
<div class="row">             
    <div class="col-xs-12 tuotteet">
<?php
        if($_SERVER["REQUEST_METHOD"] == "GET") {
            $id=filter_input(INPUT_GET,'tuoteryhma_id',FILTER_SANITIZE_NUMBER_INT);
            if($id != 0) {
                /* Tuoteryhma id tuli GET parametrina ja se laitetaan sesiolle talteen paluuta varten. */
                $_SESSION['tuoteryhma_id']=$id;
                }
            else {
                /* Tuoteryhma id ei tullut GET parametri joten tarkista se sessiosta tai sitten lue tietokannan ensimmäinen ryhmä. */
                if($tietokanta != null) {
                    /* Tarkista onko tuoteryhma id jo tallennettu sessioon */
                    if(isset($_SESSION['tuoteryhma_id'])) 
                        {
                        /* Luetaan id sessiosta. */
                        $id=$_SESSION['tuoteryhma_id'];
                        }
                    else 
                        {
                        /* Id:tä ei ollut sessiossa joten luetaan tietokannan ensimmäinen ryhmä. */
                        try {
                            $sql="SELECT * FROM tuoteryhma";
                            $kysely=$tietokanta->query($sql);
                            $kysely->setFetchMode(PDO::FETCH_OBJ);

                            $tietue=$kysely->fetch();
                            $id=$tietue->id;
                        } catch (Exception $pdoex) {
                            /* Kirjoita virhe ilmoitus. */
                            ?>
                            <div class="alert alert-danger" role="alert">
                              <span class="sr-only"></span>
                                Tietokannan luennassa tapahtui virhe.
                                <br />
                            </div>
                            <?php
                            print $pdoex->getMessage();
                            }
                        }
                    }
                }
            }
?>        
        <div class="row">
            <div class="col-xs-12">
<?php
            if($tietokanta != null) {
                try {
                    $sql="SELECT * FROM tuoteryhma WHERE id=$id";
                    $kysely=$tietokanta->query($sql);
                    $kysely->setFetchMode(PDO::FETCH_OBJ);

                    $tietue=$kysely->fetch();
                    print "<h4>$tietue->nimi</h4>";
                } catch (Exception $pdoex) {
                    /* Kirjoita virhe ilmoitus. */
                    ?>
                    <div class="alert alert-danger" role="alert">
                      <span class="sr-only"></span>
                        Tietokannan luennassa tapahtui virhe.
                        <br />
                    <?php
                    print $pdoex->getMessage();
                    ?>
                    </div>
                    <?php
                    }
                }
?>                
                <hr>
            </div>
        </div> 
        <div class="row">
<?php
            /* Lue tuotteryhman tuotteet tietokannasta */
            if($tietokanta != null) {
                try {
                    $sql="SELECT * FROM tuote WHERE tuoteryhma_id=$id";
                    $kysely=$tietokanta->query($sql);
                    $kysely->setFetchMode(PDO::FETCH_OBJ);

                    while($tietue=$kysely->fetch()) {
                        print "<div class='col-xs-6 col-sm-4 col-md-3 tuote'>";
                        print "<p>";
                        print "<a href='tuotetiedot.php?tuote_id=$tietue->id'><img src='tuotteet/$tietue->kuva'/></a>";
                        print "<br />";
                        print "$tietue->nimi<br />";
                        print "$tietue->hinta eur";
                        print "</p>";
                        print "</div>";
                    }
                } catch (Exception $pdoex) {
                    /* Kirjoita virhe ilmoitus. */
                    ?>
                    <div class="alert alert-danger" role="alert">
                      <span class="sr-only"></span>
                        Tuoteiden luennassa tapahtui virhe.
                        <br />
                    <?php
                    print $pdoex->getMessage();
                    ?>
                    </div>
                    <?php
                    }
                }            
?>            
        </div>
    </div>
</div>   
<?php include_once 'inc/bottom.php'; ?>