<?php include_once 'inc/top.php'; ?>

<?php
if($tietokanta != null)
    {
    try {
        $sql='SELECT tilaus.id, asiakas.sukunimi, asiakas.etunimi, tilaus.aika
        FROM tilaus
        INNER JOIN asiakas
        ON tilaus.asiakas_id=asiakas.id;';        
        
        $kysely=$tietokanta->query($sql);
        $kysely->setFetchMode(PDO::FETCH_OBJ);

        print "<h4>Tilaukset</h4>";
        print "<hr>";
        print "<table class='table'>";
        print "<thead>";
        print " <tr>";
        print "  <th>Tilaus</th>";
        print "  <th>Sukunimi</th>";
        print "  <th>Etunimi</th>";
        print "  <th>Aika</th>";
        print "  <th>Tuotteet</th>";
        print " </tr>";
        print "</thead>";
        print "<tbody>";
        
        while($tilaus = $kysely->fetch()) {
            
            print '<tr>';
            print '<td>' . $tilaus->id . '</td>';
            print '<td>' . $tilaus->sukunimi . '</td>';
            print '<td>' . $tilaus->etunimi . '</td>';
            print '<td>' . date("d.m.Y H:i", strtotime($tilaus->aika)) . '</td>';
           
            $sql="SELECT tilausrivi.tuote_id, tuote.nimi
            FROM tilausrivi
            INNER JOIN tuote 
            ON tuote.id=tilausrivi.tuote_id
            WHERE tilaus_id='$tilaus->id'";

            $kysely2=$tietokanta->query($sql);
            $kysely2->setFetchMode(PDO::FETCH_OBJ);
            $i=1;
            while($tuote = $kysely2->fetch()) {
                print '<td>' . $tuote->nimi . '</td>';
                print '</tr>';
                if($i<($kysely2->rowCount())) {
                    print '<tr>';
                    print '<td></td>';
                    print '<td></td>';
                    print '<td></td>';
                    print '<td></td>';
                    $i++;
                    }
                }
            }
        print "</tbody>";
        print "</table>";

    } catch (Exception $pdoex) {
        ?>
        <div class="alert alert-danger" role="alert">
          <span class="sr-only"></span>
            Kaikkia tilauksia ei saatu luettua!
            <br />
            <?php
            print $pdoex->getMessage();
            ?>
        </div>
        <?php        
        }
    }
?>
    
<?php include_once 'inc/bottom.php'; ?>

