<?php include_once 'inc/top.php'; ?>

<?php 
    if($_SERVER["REQUEST_METHOD"] == "POST" && $tietokanta != null) /* Tarkista onko tietokannan avaus onnistunut */
        {
        try {
            $nimi=filter_input(INPUT_POST,'nimi',FILTER_SANITIZE_STRING);
            /* prepare kutsua ei voida käyttää jos halua erotella getCode() arvot ja käsittelyn.
            $kysely = $tietokanta->prepare("INSERT INTO tuoteryhma (nimi) "
                    . "VALUES (:nimi)");
            $kysely->bindValue(':nimi', $nimi, PDO::PARAM_STR);
            $kysely->execute();
             */
            $tulos=$tietokanta->exec("INSERT INTO tuoteryhma(nimi)"
                    . "VALUES('$nimi')");
            
            /* Tiedon tallennus onnistui --> Näytä SUCCESS viesti käyttäjälle */
            ?>
            <div class="alert alert-success" role="alert">
              <span class="sr-only"></span>                    
                Tuoteryhmä <?php print "$nimi";?> tallennettu.
            </div>
            <?php

        } catch (PDOException $pdoex) {
            /* Tiedon tallennus epäonnistui --> tarkista syy */
            if($pdoex->getCode() == 23000) { /* ryhmä on jo luotu --> näytä INFO viesti */
                ?>
                <div class="alert alert-warning" role="alert">
                <span class="sr-only"></span>                    
                Antamasi (<?php print "$nimi";?>) tuoteryhmä on jo tietokannassa!
                </div>
                <?php
            }
            else { /* Tiedon tallennus epäonnitui --> näytä DANGER viesti */
            ?>
            <div class="alert alert-danger" role="alert">
              <span class="sr-only"></span>
                Tuoteryhmän tallennuksessa tapahtui virhe,
                <br />
                <?php
                print $pdoex->getMessage();
                ?>
            </div>
            <?php
            }
          }
        }
    ?>
    <h3>Lisää tuoteryhmä</h3>
    <form role="form" action='<?php print($_SERVER['PHP_SELF']); ?>' method="post">
      <div class="form-group">
        <label for="Nimi">Nimi:</label>
        <input type="text" class="form-control" name="nimi" max="50" autofocus>
      </div>
      <button type="submit" class="btn btn-primary">Tallenna</button>
      <input type="button" class="btn btn-default" onclick="window.location='index.php';return false;" value='Peruuta'>
    </form>
<?php include_once 'inc/bottom.php'; ?>