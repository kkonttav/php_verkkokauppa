<?php include_once 'inc/top.php'; ?>

<?php
    if($_SERVER["REQUEST_METHOD"] == "POST" && $tietokanta != null)
        {
        $id=$tuote_id=filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT);
        try {
            $sql="SELECT * FROM tuote WHERE id=$id";
            $kysely=$tietokanta->query($sql);
            $kysely->setFetchMode(PDO::FETCH_OBJ);
            $tietue=$kysely->fetch();
            
            $_SESSION['ostoskori'][count($_SESSION['ostoskori'])] = $tietue; /* Tallenna taulukon viimeiseksi */
        } catch (Exception $pdoex) {
            /* Kirjoita virhe ilmoitus. */
            ?>
            <div class="alert alert-danger" role="alert">
              <span class="sr-only"></span>
                Tuotetta ei voitu lisätä ostoskoriin.
                <br />
            <?php
            print $pdoex->getMessage();
            ?>
            </div>
            <?php
            }
        }
        ?>
        <!--- laita '6' kolummi ennen Ostoskori-nappia niinkuin index sivulla on (Tuotteet)-nappilla --->
        <div class="col-xs-6"></div>
        <?php
        include_once 'kori.php';
        $tuote_id=filter_input(INPUT_GET,'tuote_id',FILTER_SANITIZE_NUMBER_INT);
        if($tuote_id == 0) {
            /* Sivulle tultiin ilman GET parametria --> Näytä virhe ilmoitus. */
            ?>
            <div class="alert alert-danger" role="alert">
              <span class="sr-only"></span>
                Tuotetta ei voida näyttää.
                <br />
            </div>
            <?php            
            }
        else {
            /* Lue tuoteen tiedot tietokannasta annetulla parametrilla*/
            if($tietokanta != null) {
                try {
                    $sql="SELECT * FROM tuote WHERE id=$tuote_id";
                    $kysely=$tietokanta->query($sql);
                    $kysely->setFetchMode(PDO::FETCH_OBJ);

                    while($tietue=$kysely->fetch()) {
                        print "<div class='row'>";
                            print "<div class='col-xs-12 tuote'>";
                                print "<div class='row'>";
                                    print "<div class='col-xs-12'>";
                                        print "<h4>$tietue->nimi</h4>";
                                        print "<hr>";
                                    print "</div>";
                                print "</div>";                
                                print "<div class='col-xs-6 col-md-6 tuotekuva'>";
                                    print "<img class='center-block' src='tuotteet/$tietue->kuva'/>";
                                print "</div>";
                                print "<div class='col-xs-3 col-md-3 tuotetiedot'>";
                                    print "$tietue->nimi ";
                                    print "$tietue->hinta €";
                                    print "<br />";
                                    print "<br />";
                                    print "$tietue->kuvaus<br />";
                                print "</div>";
                                print "<div class='col-xs-3 col-md-3 tuotenapit'>";
                                    print "<form role='form' method='post'>";
                                        print "<input name='id' value='$tietue->id' hidden>";
                                        print "<button type='submit' class='btn btn-primary'>Lisää ostoskoriin</button>";
                                    print "</form>";
                                    print "<br /><br />";
                                    print "<a type=btn class='btn btn-default' href='index.php'>Takaisin tuotteisiin</a>";
                                print "</div>";
                            print "</div>";
                        print "</div>";
                    }
                } catch (Exception $pdoex) {
                    /* Kirjoita virhe ilmoitus. */
                    ?>
                    <div class="alert alert-danger" role="alert">
                      <span class="sr-only"></span>
                        Tuoteiden luennassa tapahtui virhe.
                        <br />
                    <?php
                    print $pdoex->getMessage();
                    ?>
                    </div>
                    <?php
                    }
                }
            }
?>  
<?php include_once 'inc/bottom.php'; ?>